# Atlassian BitBucket Server REST API Scripts

This project contains Unix shell scripts to achieve select
administrative operations on a BitBucket repository using the
[Atlassian REST API](https://docs.atlassian.com/bitbucket-server/rest/6.9.0/bitbucket-rest.html).

- `addusers.sh` promotes or demotes one or many users' permission
  level for a repository.

- `delusers.sh` revokes all permissions of one or many users for a
  repository.
  
- `getusers.sh` lists the names of users that have been granted at
  least one permission for a repository.
  
- `addrepos.sh` creates new repositories in an existing BitBucket
  project.
  
- `delrepos.sh` deletes repositories in an existing BitBucket project.

- `getrepos.sh` lists repositories in an existing BitBucket project.

- `getprojects.sh` lists visible projects in BitBucket server.
  
- `delprojects.sh` deletes an empty BitBucket project (only one at a
  time despite the name).

The scripts require `curl`. They are developed, tested and used on
macOS.

## Author

Vincent Goulet, École d'actuariat, Université Laval

## Version

1.1 (2021-07-19)

## Obtaining the scripts

Get the `.zip` archive of the latest stable release of the scripts
from the [releases page](https://gitlab.com/vigou3/bitbucket-scripts/releases) on GitLab.

## Source code
 
https://gitlab.com/vigou3/bitbucket-scripts

## Disclaimer

The scripts were written for my own use and they serve me well. They
are provided here in case they may be useful to anyone.

Use at your own risk.
