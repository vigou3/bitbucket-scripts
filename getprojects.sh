#!/bin/sh

## Copyright (C) 2021 Vincent Goulet
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>. 

SCRIPTNAME=$(basename -- "$0")
DOC_REQUEST=70

if [ $# -eq 0 ]
then
    cat <<USAGEXX
Usage: ${SCRIPTNAME} [options...] <url>
-h, --help                Show complete help text
-l <n>, --page-limit=<n>  Set the limit parameter of the REST API
[...]                     List of curl options
USAGEXX
    exit ${DOC_REQUEST}
fi

if [ "$1" = "-h" ] || [ "$1" = "--help" ]
then
    cat <<MANPAGEXX
NAME

${SCRIPTNAME} - list the keys of BitBucket projects

SYNOPSYS

${SCRIPTNAME} [-h|--help] [-l <n>|--page-limit=<n>] [<curl-options>]
            [--] <url>

DESCRIPTION

List the keys of the projects visible for the authenticated user in
the BitBucket server at url (prefix to the REST API) 'url' using the
Atlassian Bitbucket REST API.

The following options are available:

-h, --help
       Show this help text.

-l <n>, --page-limit=<n>
       Set to n the 'limit' parameter of the REST API indicating how
       many results to return per page. Most APIs default to returning
       25 if the limit is left unspecified, which may be too low if
       the number of projects is large. The default for this script is
       500 or the value of the BITBUCKET_PAGE_LIMIT environment
       variable.

<curl options>
       All other options are passed to curl unchanged. Defaults to '-n
       -s'.

ENVIRONMENT

BITBUCKET_PAGE_LIMIT  Default page limit to use in queries.

REFERENCE

Atlassian Bitbucket REST API Reference
https://docs.atlassian.com/bitbucket-server/rest/6.9.0/bitbucket-rest.html
MANPAGEXX
    exit ${DOC_REQUEST}
fi

## Default values.
CURLOPTIONS="-s -n"
PAGELIMIT=${BITBUCKET_PAGE_LIMIT-500}

## Process options.
while [ $# -gt 0 ]
do
    case "$1" in
	-l)
	    PAGELIMIT="$2"; shift;;
	--page-limit=*)
	    PAGELIMIT="${1#*=}";;
	--*|-*)
	    case "$2" in
                --|-*) CURLOPTIONS="${CURLOPTIONS} $1";;
                *)     CURLOPTIONS="${CURLOPTIONS} $1 $2"; shift;;
            esac ;;
	*)
	    break;;		# terminate while loop
    esac
    shift
done

## BitBucket server url is now the script's first argument.
MACHINE="$1"; shift

## Script requires a project key in argument.
! ${MACHINE:+false} || { echo "missing server url" >&2; exit 1; }

## Build REST API url.
URL="https://${MACHINE}/rest/api/1.0/projects?limit=${PAGELIMIT}"

## Create files to hold stdout and stderr of curl.
jsondata=$(mktemp)
http_code=$(mktemp)

## Retrieve the JSON page of projects. The numerical response of the
## retrieved HTTP transfer is written to stderr in order to display a
## useful message in case of error.
if curl ${CURLOPTIONS} -w "%{stderr}%{http_code}" "${URL}" >"${jsondata}" 2>"${http_code}"
then
    case $(cat "${http_code}") in
	200)
	    ;;
	400)
	    echo "permission level unknown or not related to projects" >&2
    esac
else
    echo "internal curl error" >&2
    exit 1
fi

## Extract only projects names from JSON data.
grep -E -o '"key":[^,]*' "${jsondata}" | \
    awk 'BEGIN { FS = "\"" } { print $4 }'

exit 0
