### -*-Makefile-*- to release "Atlassian BitBucket Server REST API
###                Scripts"
##
## Copyright (C) 2020 Vincent Goulet
##
## 'make zip' builds the archive.
##
## 'make release' creates a release in GitLab and uploads the archive.
##
## 'make all' is equivalent to 'make zip' to avoid accidendal
## releases.
##
## Author: Vincent Goulet
##
## This file is part of the project "Atlassian BitBucket Server REST
## API Scripts"
## https://gitlab.com/vigou3/bitbucket-scripts


## Files
ARCHIVE = bitbucket-scripts.zip
SCRIPTS = $(wildcard *.sh)
README = README.md
NEWS = NEWS
LICENSE = LICENSE

## Repository url and version string read from the README file
REPOSURL = $(shell awk '/^\#\# Source/ { getline; getline; print $$0 }' ${README})
VERSION = $(shell awk '/^\#\# Version/ { getline; getline; print $$0 }' ${README})
YEAR=${subst (,,$(word 2,$(subst -, ,${VERSION}))}

## Toolset
CP = cp -p
RM = rm -rf

## Temporary directory to build the archive
BUILDDIR = builddir

## Dépôt GitLab et authentification
REPOSNAME = $(shell basename ${REPOSURL})
APIURL = https://gitlab.com/api/v4/projects/vigou3%2F${REPOSNAME}
OAUTHTOKEN = $(shell cat ~/.gitlab/token)

## Variables automatiques
TAGNAME = v$(firstword ${VERSION})


all: zip

.PHONY: release
release: update-copyright zip check-status upload create-release

.PHONY: update-copyright
update-copyright: ${SCRIPTS}
	for f in $?; \
	    do sed -E '/^#* +Copyright \(C\)/s/20[0-9]{2}/${YEAR}/' \
	           $$f > $$f.tmp && \
	           ${CP} $$f.tmp $$f && \
	           ${RM} $$f.tmp && \
		   chmod u+x $$f; \
	done

.PHONY: zip
zip: ${SCRIPTS} ${README} ${NEWS} ${LICENSE}
	zip --filesync ${ARCHIVE} $^

.PHONY: check-status
check-status:
	@echo ----- Checking status of working directory...
	@if [ "master" != $(shell git branch --list | grep ^* | cut -d " " -f 2-) ]; then \
	     echo "not on branch master"; exit 2; fi
	@if [ -n "$(shell git status --porcelain | grep -v '^??')" ]; then \
	     echo "uncommitted changes in repository; not creating release"; exit 2; fi
	@if [ -n "$(shell git log origin/master..HEAD | head -n1)" ]; then \
	    echo "unpushed commits in repository; pushing to origin"; \
	     git push; fi

.PHONY: upload
upload :
	@echo ----- Uploading archive to GitLab...
	$(eval upload_url=$(shell curl --form "file=@${ARCHIVE}" \
	                                        --header "PRIVATE-TOKEN: ${OAUTHTOKEN}"	\
	                                        --silent \
	                                        ${APIURL}/uploads \
	                                   | awk -F '"' '{ print $$8 }'))
	@echo url to file:
	@echo "${upload_url}"
	@echo ----- Done uploading files

.PHONY: create-release
create-release :
	@echo ----- Creating release on GitLab...
	if [ -e relnotes.in ]; then rm relnotes.in; fi
	touch relnotes.in
	awk 'BEGIN { ORS = " "; print "{\"tag_name\": \"${TAGNAME}\"," } \
	      /^$$/ { next } \
	      (state == 0) && /^# / { \
		state = 1; \
		out = $$2; \
	        for(i = 3; i <= NF; i++) { out = out" "$$i }; \
	        printf "\"name\": \"Version %s\", \"description\":\"", out; \
	        next } \
	      (state == 1) && /^# / { exit } \
	      state == 1 { printf "%s\\n", $$0 } \
	      END { print "\",\"assets\": { \"links\": [{ \"name\": \"${ARCHIVE}\", \"url\": \"${REPOSURL}${upload_url}\" }] }}" }' \
	     ${NEWS} >> relnotes.in
	curl --request POST \
	     --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	     "${APIURL}/repository/tags?tag_name=${TAGNAME}&ref=master"
	curl --request POST \
	     --data @relnotes.in \
	     --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	     --header "Content-Type: application/json" \
	     --output /dev/null --silent \
	     ${APIURL}/releases
	${RM} relnotes.in
	@echo ----- Done creating the release
