#!/bin/sh

## Copyright (C) 2021 Vincent Goulet
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>. 

SCRIPTNAME=$(basename -- "$0")
DOC_REQUEST=70

if [ $# -eq 0 ]
then
    cat <<USAGEXX
Usage: ${SCRIPTNAME} [options...] <projectkey> <repository>
    --bitbucketrc-file <filename> Specify FILE for bitbucketrc file
-h, --help                 Show complete help text
-l <n>, --page-limit=<n>   Set the limit parameter of the REST API
-m <url>, --machine=<url>  Set BitBucket server url
[...]                      List of curl options
    --                     Mark the end of options
USAGEXX
    exit ${DOC_REQUEST}
fi

if [ "$1" = "-h" ] || [ "$1" = "--help" ]
then
    cat <<MANPAGEXX
NAME

${SCRIPTNAME} - list users of a repository in a BitBucket project

SYNOPSYS

${SCRIPTNAME} [--bitbucketrc-file <filename>] [-h|--help]
            [-l <n>|--page-limit=<n>] [-m <url>|--machine=<url>]
            [<curl-options>] [--] <projectkey> <repository>

DESCRIPTION

List the names of users that have been granted at least one permission
for the repository with slug 'repository' in the BitBucket project
with key 'projectkey' using the Atlassian Bitbucket REST API.

By default, the BitBucket server url is read from the '.bitbucketrc'
file in the user's home directory.

The following options are available:

--bitbucketrc-file <filename>
       Path (absolute or relative) to the bitbucketrc file that the
       script should use (instead of the default '~/.bitbucketrc').

-h, --help
       Show this help text.

-l <n>, --page-limit=<n>
       Set to n the 'limit' parameter of the REST API indicating how
       many results to return per page. Most APIs default to returning
       25 if the limit is left unspecified, which may be too low if
       the number of users is large. The default for this script is
       500 or the value of the BITBUCKET_PAGE_LIMIT environment
       variable.

-m <url>, --machine=<url>
       BitBucket server url (prefix to the REST API). Overrides the
       default '~./bitbucketrc' file and option --bitbucketrc-file.

<curl options>
       All other options are passed to curl unchanged. Defaults to '-n
       -s'. This script's option '-m' mask the one of curl. Use the
       long forms of these options to pass them to curl.

--
       Marks the end of options, notably curl ones.

FILES

~/.bitbucketrc
       The structure of the file is similar to a '.netrc' file as used
       by curl for authentication.

       An example for a project hosted on Université Laval's Faculté
       des sciences et de génie BitBucket server is

       machine projets.fsg.ulaval.ca/git

       This would result in a call to the REST API located at

       https://projets.fsg.ulaval.ca/git/rest/api/1.0/projects/<projectkey>/repos/<repository>

ENVIRONMENT

BITBUCKET_PAGE_LIMIT  Default page limit to use in queries.

REFERENCE

Atlassian Bitbucket REST API Reference
https://docs.atlassian.com/bitbucket-server/rest/6.9.0/bitbucket-rest.html
MANPAGEXX
    exit ${DOC_REQUEST}
fi

## Default values.
MACHINE=
CURLOPTIONS="-s -n"
BITBUCKETRC=~/.bitbucketrc
PAGELIMIT=${BITBUCKET_PAGE_LIMIT-500}

## Process options.
while [ $# -gt 0 ]
do
    case "$1" in
	-l)
	    PAGELIMIT="$2"; shift;;
        --page-limit=*)
            PAGELIMIT="${1#*=}";;
	-m)
	    MACHINE="$2"; shift;;
	--machine=*)
	    MACHINE="${1#*=}";;
	--bitbucketrc-file)
	    BITBUCKETRC="$2"; shift;;
	--)
	    shift; break;;
	--*|-*)
	       case "$2" in
                   --|-*) CURLOPTIONS="${CURLOPTIONS} $1";;
                   *)     CURLOPTIONS="${CURLOPTIONS} $1 $2"; shift;;
               esac ;;
	*)
	    break;;		# terminate while loop
    esac
    shift
done

## Project key is now the script's first argument.
PROJECTKEY="$1"; shift

## Script requires a project key in argument.
! ${PROJECTKEY:+false} || { echo "missing project key" >&2; exit 1; }

## Repository slug is now the script's first argument.
REPOSITORYSLUG="$1"; shift

## Script requires a repository slug in argument.
! ${REPOSITORYSLUG:+false} || { echo "missing repository slug" >&2; exit 1; }

## If the BitBucket server url is not set with option -m, read it from
## the `bitbucketrc` file.
if [ -z "${MACHINE}" ]
then
    MACHINE=$(grep -o 'machine [^ ]*' "${BITBUCKETRC}" | cut -d ' ' -f 2)
fi

## Build REST API url.
URL="https://${MACHINE}/rest/api/1.0/projects/${PROJECTKEY}/repos/${REPOSITORYSLUG}/permissions/users?limit=${PAGELIMIT}"

## Create files to hold stdout and stderr of curl.
jsondata=$(mktemp)
http_code=$(mktemp)

## Retrieve the JSON page of users. The numerical response of the
## retrieved HTTP transfer is written to stderr in order to display a
## useful message in case of error.
if curl ${CURLOPTIONS} -w "%{stderr}%{http_code}" "${URL}" >"${jsondata}" 2>"${http_code}"
then
    case $(cat "${http_code}") in
	200)
	    ;;
	401)
	    echo "authenticated user is not a repository administrator for the repository" >&2
	    exit 1;;
	404)
	    echo "repository does not exist" >&2
	    exit 1;;
    esac
else
    echo "internal curl error" >&2
    exit 1
fi

## Extract only user names from JSON data.
grep -E -o '"name":[^,]*' "${jsondata}" | \
    awk 'BEGIN { FS = "\"" } { print $4 }'

exit 0
