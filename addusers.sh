#!/bin/sh

## Copyright (C) 2021 Vincent Goulet
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>. 

SCRIPTNAME=$(basename -- "$0")
DOC_REQUEST=70

if [ $# -eq 0 ]
then
    cat <<USAGEXX
Usage: ${SCRIPTNAME} [options...] <projectkey> <repository> [<user>]
     --bitbucketrc-file <filename> Specify FILE for bitbucketrc file
-h,  --help                 Show complete help text
-m <ulr>,  --machine=<url>  Set BitBucket server url
-q,  --quiet                Quiet mode
-ro, --read-only            Grant read-only permission
-rw, --read-write           Grant read-write permission
[...]                       List of curl options
     --                     Mark the end of options
USAGEXX
    exit ${DOC_REQUEST}
fi

if [ "$1" = "-h" ] || [ "$1" = "--help" ]
then
    cat <<MANPAGEXX
NAME

${SCRIPTNAME} - add users to a repository in a BitBucket project

SYNOPSYS

${SCRIPTNAME} [--bitbucketrc-file <filename>] [-h|--help]
            [-m <url>|--machine=<url>] [-q|--quiet] 
	    [-ro|--read-only] [-rw|--read-write] [<curl-options>]
	    [--] <projectkey> <repository> [<user>]

DESCRIPTION

Promote or demote the permission level of 'user' for the repository
with slug 'repository' in the BitBucket project with key 'projectkey'
using the Atlassian Bitbucket REST API.

By default, the BitBucket server url is read from the '.bitbucketrc'
file in the user's home directory.

The following options are available:

--bitbucketrc-file <filename>
       Path (absolute or relative) to the bitbucketrc file that the
       script should use (instead of the default '~/.bitbucketrc').

-h, --help
       Show this help text.

-m <url>, --machine=<url>
       BitBucket server url (prefix to the REST API). Overrides the
       default '~./bitbucketrc' file and option --bitbucketrc-file.

-q, --quiet
       Quiet mode: suppress normal output.

-ro
       Grant read-only (REPO_READ) permission. This is the default.

-rw
       Grant read and write (REPO_WRITE) permission.

<curl options>
       All other options are passed to curl unchanged. Defaults to '-n
       -s'. This script's option '-m' mask the one of curl. Use the
       long forms of these options to pass them to curl.

--
       Marks the end of options, notably curl ones.

If no user arguments are specified, the standard input is used.

FILES

~/.bitbucketrc
       The structure of the file is similar to a '.netrc' file as used
       by curl for authentication.

       An example for a project hosted on Université Laval's Faculté
       des sciences et de génie BitBucket server is

       machine projets.fsg.ulaval.ca/git

       This would result in a call to the REST API located at

       https://projets.fsg.ulaval.ca/git/rest/api/1.0/projects/<projectkey>/repos/<repository>

REFERENCE

Atlassian Bitbucket REST API Reference
https://docs.atlassian.com/bitbucket-server/rest/6.9.0/bitbucket-rest.html
MANPAGEXX
    exit ${DOC_REQUEST}
fi

## Default values.
MACHINE=
PERMISSION=REPO_READ
CURLOPTIONS="-s -n"
BITBUCKETRC=~/.bitbucketrc

showoutput=true

## Process options.
while [ $# -gt 0 ]
do
    case "$1" in
	-ro|--read-only)
	    PERMISSION=REPO_READ;;
	-rw|--read-write)
	    PERMISSION=REPO_WRITE;;
	--bitbucketrc-file)
	    BITBUCKETRC="$2"; shift;;
	-m)
	    MACHINE="$2"; shift;;
	--machine=*)
	    MACHINE="${1#*=}";;
	-q|--quiet)
	    showoutput=false;;
	--)
	    shift; break;;
	--*|-*)
	    case "$2" in
                --|-*) CURLOPTIONS="${CURLOPTIONS} $1";;
                *)     CURLOPTIONS="${CURLOPTIONS} $1 $2"; shift;;
            esac ;;
	*)
	    break;;		# terminate while loop
    esac
    shift
done

## Project key is now the script's first argument.
PROJECTKEY="$1"; shift

## Script requires a project key in argument.
! ${PROJECTKEY:+false} || { echo "missing project key" >&2; exit 1; }

## Repository slug is now the script's first argument.
REPOSITORYSLUG="$1"; shift

## Script requires a repository slug in argument.
! ${REPOSITORYSLUG:+false} || { echo "missing repository slug" >&2; exit 1; }

## If the BitBucket server url is not set with option -m, read it from
## the `bitbucketrc` file.
if [ -z "${MACHINE}" ]
then
    MACHINE=$(grep -o 'machine [^ ]*' "${BITBUCKETRC}" | cut -d ' ' -f 2)
fi

## Use the remaining script arguments or standard input for user
## names.
if [ $# -eq 0 ]
then
    [ ! -t 0 ] || { echo "missing user name(s)" >&2; exit 1; }
    USERS=$(cat -)
else
    USERS="$*"
fi

## Build REST API url.
URL="https://${MACHINE}/rest/api/1.0/projects/${PROJECTKEY}/repos/${REPOSITORYSLUG}/permissions/users"

## Create files to hold stdout and stderr of curl.
jsondata=$(mktemp)
http_code=$(mktemp)

## Add users one at a time.
for U in ${USERS}
do
    ## The numerical response of the retrieved HTTP transfer is
    ## written to stderr in order to display a useful message in case
    ## of error.
    if curl -X PUT -H "Content-Type: application/json" -d "{}" ${CURLOPTIONS} -w "%{stderr}%{http_code}" "${URL}?name=${U}&permission=${PERMISSION}" >"${jsondata}" 2>"${http_code}"
    then
	case $(cat "${http_code}") in
	    204)
		${showoutput} && echo "permission ${PERMISSION} granted to user ${U}";;
	    400)
		echo "request is malformed or specified permission does not exist"
		exit 1;;
	    401)
		echo "authenticated user is not a repository administrator for the repository" >&2
		exit 1;;
	    403)
		echo "action disallowed as it would reduce the permission level of the authenticated user" >&2
		exit 1;;
	    404)
		## there are two types of 404 return codes: no
		## repository (for which case we can bail out) and no
		## user with the current name
		grep -q 'NoSuchRepositoryException' "${jsondata}" && \
		    { echo "repository does not exist" >&2; exit 1; }
		echo "no such user: ${U}";;	
	esac
    else
	echo "internal curl error" >&2
	exit 1
    fi
done

exit 0
